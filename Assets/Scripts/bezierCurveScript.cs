using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bezierCurveScript : MonoBehaviour
{
    private LineRenderer lineRenderer;
    public Transform initialPos;
    public Transform curvePos;
    public Transform targetPos;
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    void Update()
    {
        if(targetPos!= null) // if target still exists
            DrawQuadraticBezierCurve(initialPos.position, curvePos.position, targetPos.position);
    }

    void DrawQuadraticBezierCurve(Vector3 initialPos, Vector3 curvePos, Vector3 targetPos)
    {
            lineRenderer.positionCount = 200;
            float t = 0f;
            Vector3 bezierVector = new Vector3(0, 0, 0);
            for (int i = 0; i < lineRenderer.positionCount; i++)
            {
            bezierVector = (1 - t) * (1 - t) * initialPos + 2 * (1 - t) * t * curvePos + t * t * targetPos;
                lineRenderer.SetPosition(i, bezierVector);
                t += (1 / (float)lineRenderer.positionCount);
            }
    }
}