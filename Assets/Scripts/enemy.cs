using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy : MonoBehaviour
{
    [SerializeField] private int health;
    public GameObject deathEffect;

    public void takeDamage (int damage)
    {
        health -= damage;
        
        if (health <= 0)
        {
            die();
        }
    }

    void die()
    {
        //Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
