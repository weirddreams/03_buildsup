using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyBounce : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag == "ground")
        {
            transform.parent.GetComponent<slimeJumperMovement>().noBounce();
        }
            
    }
}
