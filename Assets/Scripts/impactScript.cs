using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class impactScript : MonoBehaviour
{

    public int impactDamage; 
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "enemy")
        {
            collision.GetComponent<enemy>().takeDamage(impactDamage);
        }
    }
}
