using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class orb : MonoBehaviour
{
    //Assign a GameObject in the Inspector to rotate around
    public GameObject target;
    public float speed;
    public Vector3 velocity = Vector3.zero;
    public float randomRange, orbHigh;
    private float randomFloat;
    private void Start()
    {
        randomRange = 8f;
        orbHigh = 2f;
        speed = 40f;
    }
    void FixedUpdate()
    {
        randomFloat = Random.Range(-randomRange, randomRange);
        Vector3 targetPosition = target.transform.position + new Vector3(randomFloat, orbHigh + (randomFloat / 4), 0);
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition ,ref velocity, speed * Time.deltaTime);
    }

    public void changeTarget(GameObject newTarget)
    {
        target = newTarget;

    }
}
