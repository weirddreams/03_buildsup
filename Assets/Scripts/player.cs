using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;
public class player : MonoBehaviour
{
    PlayerControls controls;
    Rigidbody2D rb;
    [SerializeField] private float runSpeed = 1f, jumpForce, dashForce, startDashTime, attackRange, attackCooldown;
    Vector2 jumpVector;
    public bool superHeroLanding = false;
    public Animator animator, levelTransitionAnimator;
    Vector2 moveVector, dashVector;
    private float horizontalMove, dashTime, attackTime;
    private bool dashing = true, charged = false, isGrounded, shakeCamera, invincible, dashHorizontal;
    private int dashCount = 0;
    [SerializeField] private int dashMax = 1;
    SpriteTrailRenderer.SpriteTrailRenderer STR;
    public Transform shootPoint;
    public int damage;
    public cameraShake cameraShake;
    public GameObject impact, myOrb, bullet, onHitPlayer, onHitEnemy;
    public Material playerMaterial;
    public Vector3 playerCheckpointPos;
    public levelManager levelManager;

    private void Awake()
    {
        controls = new PlayerControls();
        
        controls.Gameplay.Jump.performed += ctx => jump();

        controls.Gameplay.Move.performed += ctx => moveVector = ctx.ReadValue<Vector2>();
        controls.Gameplay.Move.canceled += ctx => moveVector = Vector2.zero;

        controls.Gameplay.Move.performed += ctx => dashVector = ctx.ReadValue<Vector2>();
        controls.Gameplay.Dash.started += ctx => dashStart();

        controls.Gameplay.Attack.performed += ctx =>
        {
            if (ctx.interaction is HoldInteraction)
            {
                charged = true;
                charging();
            }
            else if (ctx.interaction is TapInteraction)
            {
                shoot();
            }

        };
       
        controls.Gameplay.Attack.canceled += ctx =>
         {
             if (ctx.interaction is HoldInteraction && charged)
             {
                 chargedShoot();
                 charged = false;
             }

         };

        controls.Gameplay.Attack1.performed += ctx =>
        {
            if (ctx.interaction is HoldInteraction)
            {
                charged = true;
                charging();
            }
            else if (ctx.interaction is TapInteraction)
            {
                shoot();
            }

        };

        controls.Gameplay.Attack1.canceled += ctx =>
        {
            if (ctx.interaction is HoldInteraction && charged)
            {
                chargedShoot();
                charged = false;
            }

        };

        controls.Gameplay.Attack2.performed += ctx =>
        {
            if (ctx.interaction is HoldInteraction)
            {
                charged = true;
                charging();
            }
            else if (ctx.interaction is TapInteraction)
            {
                shoot();
            }

        };

        controls.Gameplay.Attack2.canceled += ctx =>
        {
            if (ctx.interaction is HoldInteraction && charged)
            {
                chargedShoot();
                charged = false;
            }

        };

    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        jumpVector = new Vector2(0f, jumpForce);
        STR = GetComponent<SpriteTrailRenderer.SpriteTrailRenderer>();
        playerCheckpointPos = transform.position;
        attackTime = 0f;
    }

    private void FixedUpdate()
    {
        if(Mathf.Abs(moveVector.x) > 0.5f || moveVector.x == 0) //makes a deadzone in analog maybe just get input and fix it to minimum speed can be better
            horizontalMove = moveVector.x * runSpeed;

        if(!superHeroLanding) // lock horizontal movement while dashing
                rb.velocity = new Vector2(horizontalMove, rb.velocity.y);

        animator.SetFloat("speed", Mathf.Abs(horizontalMove));
        animator.SetFloat("animSpeed", Mathf.Abs(horizontalMove) / runSpeed);

        //rotate player according to movement
        if (horizontalMove < 0)
        {
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        }
        else if (horizontalMove > 0)
        {
            transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }

        if(rb.velocity.y < -0.001)
            animator.SetBool("isFalling", true);


        if (dashing)
        {
            animator.SetBool("isJumping", true);
            dash();
        }

        if(attackTime >= 0)
            shootCooldown();

        if(shakeCamera)
            StartCoroutine(cameraShake.shake(0.05f, 0.02f));

        if (levelTransitionAnimator.GetBool("controllerState"))
        {
            enableController();
        }
        else if (!levelTransitionAnimator.GetBool("controllerState"))
        {
            disableController();
        }

    }

    public void onLanding()
    {
        isGrounded = true;
        dashVector.y = 0;

        animator.SetBool("isJumping", false);
        animator.SetBool("isFalling", false);

        if (superHeroLanding)
        {
            StartCoroutine(superHeroLandingInvinceTimer(0.3f));
            StartCoroutine(cameraShake.shake(0.1f, 0.25f));

            //LayerMask mask = LayerMask.GetMask("Enemy");
            //Collider2D[] hitObjs = Physics2D.OverlapCircleAll(transform.position, attackRange, mask);

            //if (hitObjs == null)
            //    return;

            //for (int i = 0; i < hitObjs.Length; i++)
            //{
            //    hitObjs[i].transform.GetComponent<enemy>().takeDamage(damage);
            //}

            superHeroLanding = false;
            StartCoroutine(impactCoroutine());
        }

    }

    IEnumerator superHeroLandingInvinceTimer(float invinceTime)
    {
        yield return new WaitForSeconds(invinceTime);
        invincible = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("ground"))
        {
            onLanding();
        }
        if (collision.collider.CompareTag("enemy")) // restart
        {
            if(invincible)
                collision.collider.transform.GetComponent<enemy>().takeDamage(damage);
            else
            {
                Instantiate(onHitPlayer, collision.transform.position, Quaternion.identity);
                StartCoroutine(cameraShake.shake(0.2f, 0.25f));
                levelManager.resetLevel();
            }
           
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        dashCount = 0; // reset
    }

    void jump()
    {
        if (isGrounded)
        {
            rb.AddForce(jumpVector);
            isGrounded = false;
            animator.SetBool("isJumping", true);
            animator.SetBool("isFalling", false);
        }
    }

    void dashStart()
    {
        dashCount++;

        if(dashCount <= dashMax)
        {
            dash();
        }
    }

    void dash()
    {
        STR.enabled = true;

        dashing = true;
        if(dashTime <= 0 && !superHeroLanding || isGrounded)
        {
            dashing = false;
            dashHorizontal = false;
            dashTime = startDashTime;
            rb.velocity = Vector2.zero;
            Physics2D.gravity = new Vector2(0f, -9.8f);
          
            if(isGrounded)
                animator.SetBool("isJumping", false);

            STR.enabled = false;
        }
        else
        {
            dashTime -= Time.deltaTime;

            if (Mathf.Abs(dashVector.x) > Mathf.Abs(dashVector.y) || dashVector.y > 0 && !superHeroLanding) // left right
            {
                dashHorizontal = true;
                rb.AddForce(transform.right * dashForce);
                Physics2D.gravity = Vector2.zero;
                rb.velocity = new Vector2(rb.velocity.x, 0f);
            }
            //else if (dashVector.y > 0 && !superHeroLanding) // up
            //{

            //    rb.AddForce(transform.right * dashForce);
            //    Physics2D.gravity = Vector2.zero;
            //    rb.velocity = new Vector2(rb.velocity.x, 0f);
            //}
            else if (dashVector.y < 0 && !dashHorizontal) // down
            {
                invincible = true;
                if (rb.velocity.y > 0)
                    rb.velocity = new Vector2(0f, 0f);
                else
                    rb.velocity = new Vector2(0f, rb.velocity.y);

                Physics2D.gravity = Vector2.zero;
                    rb.AddForce(-transform.up * dashForce / 5);
                superHeroLanding = true;
            }
        }

       
    }

    void shootCooldown()
    {
        attackTime -= Time.deltaTime;

        myOrb.GetComponent<orb>().randomRange = 8f;
        myOrb.GetComponent<orb>().orbHigh = 2f;
        myOrb.GetComponent<orb>().speed = 40f;
    }

    void shoot()
    {
        if (attackTime <= 0) // ready to attack
        {
            attackTime = attackCooldown;

            LayerMask mask = LayerMask.GetMask("Enemy");
            Collider2D hitObj = Physics2D.OverlapCircle(transform.position, attackRange, mask);

            if (hitObj == null)
                return;

            hitObj.transform.GetComponent<enemy>().takeDamage(damage);

            Instantiate(onHitEnemy, hitObj.transform.position, Quaternion.identity);

            GetComponent<bezierCurveScript>().targetPos = hitObj.transform;
            StartCoroutine(bezierCurveShoot(hitObj));
            StartCoroutine(cameraShake.shake(0.1f, 0.15f));


        }
        else
        {
            myOrb.GetComponent<orb>().randomRange = 1f;
            myOrb.GetComponent<orb>().orbHigh = 3f;
            myOrb.GetComponent<orb>().speed = 10f;
        }
    }

    void charging()
    {
        //orb gets closer and twitchy
        myOrb.GetComponent<orb>().randomRange = 1f;
        myOrb.GetComponent<orb>().orbHigh = 0f;
        myOrb.GetComponent<orb>().speed = 10f;

        shakeCamera = true;

        playerMaterial.SetColor("_color", Color.red);
        SetCustomMaterialEmissionIntensity(playerMaterial, 10);
    }
    void chargedShoot()
    {
        shakeCamera = false;

        //orb throwed to top for impact and gets back to normal
        myOrb.GetComponent<orb>().randomRange = 8f;
        myOrb.GetComponent<orb>().orbHigh = 2f;
        myOrb.GetComponent<orb>().speed = 40f;
        myOrb.GetComponent<Rigidbody2D>().AddForce((Vector3.up - transform.right) * 1000);

        playerMaterial.SetColor("_color", Color.white);
        SetCustomMaterialEmissionIntensity(playerMaterial, 0);

        Instantiate(bullet, shootPoint.transform.position, shootPoint.rotation);

        StartCoroutine(cameraShake.shake(0.1f, 0.25f));

       
    }

    public void SetCustomMaterialEmissionIntensity(Material material, float intensity) // edit it to make it usable for later projects
    {
        Material mat = material;
        Color color = mat.GetColor("_color"); //probobly "_Color"

        // for some reason, the desired intensity value (set in the UI slider) needs to be modified slightly for proper internal consumption
        float adjustedIntensity = intensity - (0.4169F);

        // redefine the color with intensity factored in - this should result in the UI slider matching the desired value
        color *= Mathf.Pow(2.0F, adjustedIntensity);
        mat.SetColor("_color", color); // probably "_EmissionColor"
        //Debug.Log("<b>Set custom emission intensity of " + intensity + " (" + adjustedIntensity + " internally) on Material: </b>" + mat);
    }

    public IEnumerator bezierCurveShoot(Collider2D hit)
    {
        GetComponent<LineRenderer>().enabled = true;
        GetComponent<bezierCurveScript>().targetPos = hit.transform;
        yield return new WaitForSeconds(0.1f);
        GetComponent<LineRenderer>().enabled = false;
    }
    public IEnumerator impactCoroutine()
    {
        bool isWallLeft = false;
        bool isWallRight = false;

        for (int i = 0; i < 4; i++)
        {
            Vector3 right = transform.position + new Vector3((float)i, 0f, 0f);
            Vector3 left = transform.position - new Vector3((float)i, 0f, 0f);

            LayerMask mask = LayerMask.GetMask("Tile");
            Collider2D hitWall = Physics2D.OverlapCircle(left, 0.1f, mask);


            if (Physics2D.OverlapCircle(left, 0.1f, mask))
                isWallLeft = true;
            if (Physics2D.OverlapCircle(right, 0.1f, mask))
                isWallRight = true;

            if (!isWallLeft)
                Instantiate(impact, left, Quaternion.identity);
            if(!isWallRight)
                Instantiate(impact, right, Quaternion.identity);

            yield return new WaitForSeconds(0.05f);
        }
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }
    private void OnEnable()
    {
        controls.Gameplay.Enable();
    }

    private void OnDisable()
    {
        controls.Gameplay.Disable();
    }

    public void enableController()
    {
        controls.Gameplay.Enable();
    }
    public void disableController()
    {
        controls.Gameplay.Disable();
    }
}
