 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{
    public Animator animator;
    public float runSpeed = 1f, jumpForce =500f;
    //float horizontalMove = 0f;
    //bool isGrounded;
    Rigidbody2D rb;
    Vector2 jumpVector;


    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        jumpVector = new Vector2(0f, jumpForce);
    }
    // Update is called once per frame
    void Update()
    {
        //horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        //animator.SetFloat("speed", Mathf.Abs(horizontalMove));

        //rb.velocity = new Vector2(horizontalMove, rb.velocity.y);

        ////rotate player according to movement
        //if (horizontalMove < 0)
        //{
        //    transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        //}
        //else if (horizontalMove > 0)
        //{
        //    transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        //}


        //if (Input.GetKeyDown(KeyCode.W) && isGrounded == true)
        //{
        //    rb.AddForce(jumpVector);
        //    isGrounded = false;
        //    animator.SetBool("isJumping", true);
        //}

    }


    public void onLanding()
    {
        //isGrounded = true;
        animator.SetBool("isJumping", false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "ground")
        {
            onLanding();
        }
    }
}
