using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateEnemy : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "wall")
        {
            rotateParent();
        }
    }
    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == "ground")
        {
            rotateParent();            
        }
    }

    public void rotateParent()
    {
        transform.parent.RotateAround(transform.position, transform.up, 180f);
    }

}
