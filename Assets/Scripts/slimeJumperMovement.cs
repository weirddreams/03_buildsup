using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class slimeJumperMovement : MonoBehaviour
{
    [SerializeField] private float jumpHeight, startJumpTime, jumpRange;
    private Rigidbody2D rb;
    public bool isGrounded;
    public Animator animator;
    private BoxCollider2D boxCollider;
    private float jumpTime;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();

        isGrounded = true;
        slimeJump();
    }

    // Update is called once per frame
    void Update()
    {
        slimeJump();
    }

    void slimeJump()
    {
        LayerMask mask = LayerMask.GetMask("Player");
        Collider2D hitObj = Physics2D.OverlapCircle(transform.position, jumpRange, mask);

        if (jumpTime <= 0 && hitObj) // time is over
        {
            jumpTime = startJumpTime;

            float distanceFromPlayer_x = hitObj.transform.position.x - transform.position.x;
            float distanceFromPlayer_y = hitObj.transform.position.y - transform.position.y;

            if (isGrounded)
            {
                boxCollider.sharedMaterial = new PhysicsMaterial2D()
                {
                    bounciness = 1,
                    friction = 0.4f // default friction
                };

                rb.AddForce(new Vector2(distanceFromPlayer_x, jumpRange), ForceMode2D.Impulse);
                animator.SetBool("isJumping", true);
                isGrounded = false;
            }
        }
        else
        {
            jumpTime -= Time.deltaTime;
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "ground")
        {
            isGrounded = true;
            animator.SetBool("isJumping", false);
        }
    }
    public void noBounce()
    {
        GetComponent<BoxCollider2D>().sharedMaterial = new PhysicsMaterial2D()
        {
            bounciness = 0,
            friction = 1
        };
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, jumpRange);
    }
}