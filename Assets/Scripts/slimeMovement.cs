using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class slimeMovement : MonoBehaviour
{
    [SerializeField] private float speed;

    private void Update()
    {
        constantMovement(speed);
    }

    void constantMovement(float speed)
    {
        transform.Translate(Vector3.right * Time.deltaTime * speed);
    }
}
