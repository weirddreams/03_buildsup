using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class batMovement : MonoBehaviour
{
    [SerializeField] private float chaseRange, chaseSpeed, giveUpRange;
    public Animator animator;
    private float distanceFromTarget;
    private GameObject player;
    private bool chaseStart = false, giveUp = false;
    private float distanceChase;
    private Vector3 startPos;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        startPos = transform.position;
    }
    // Update is called once per frame
    void Update()
    {
        chase();
    }

    void chase()
    {
        distanceChase = Vector3.Distance(transform.position, player.transform.position);

        if (distanceChase < chaseRange)
        {
            chaseStart = true;
            giveUp = false;
        }

        if(distanceChase >= giveUpRange)
        {
            chaseStart = false;
            giveUp = true;
        }

        if (chaseStart)
        {
            animator.SetBool("seenPlayer", true);
            distanceFromTarget = player.transform.position.x - transform.position.x;
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, chaseSpeed * Time.deltaTime);

            if (distanceFromTarget < 0)
            {
                transform.rotation = Quaternion.Euler(0f, 180f, 0f);
            }
            else
                transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }

        if (giveUp)
        {
            distanceFromTarget = startPos.x - transform.position.x;
            transform.position = Vector3.MoveTowards(transform.position,startPos, chaseSpeed * Time.deltaTime);

            if (distanceFromTarget < 0)
            {
                transform.rotation = Quaternion.Euler(0f, 180f, 0f);
            }
            else
                transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }

        if(transform.position == startPos)
        {
            animator.SetBool("seenPlayer", false);
            transform.Rotate(180f, 0f, 0f);
        }
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, chaseRange);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, giveUpRange);
    }
}
