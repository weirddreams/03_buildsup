using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossScript : MonoBehaviour
{
    public GameObject orb;
    // Start is called before the first frame update
    void Start()
    {
        orb = GameObject.FindGameObjectWithTag("orb");
        orb.GetComponent<orb>().changeTarget(gameObject);
    }
}
