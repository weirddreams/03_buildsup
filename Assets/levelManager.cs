using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelManager : MonoBehaviour
{
    public static int level;
    [SerializeField] private GameObject player, currentLevel, orb;
    [SerializeField] private GameObject[] levelArray;
    public Animator transition;
    private Vector3 doorPos;

    void Start()
    {
        level = 0;   
        currentLevel = Instantiate(levelArray[level], new Vector3(0f, 0f, 0f), Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void resetLevel()
    {
        StartCoroutine(loadLevel(level));
        //Destroy(currentLevel);
        //currentLevel = Instantiate(levelArray[level], new Vector3(0f, 0f, 0f), Quaternion.identity);
        //player.transform.position = player.GetComponent<player>().playerCheckpointPos;
    }

    public void nextLevel()
    {
        if(level + 1 == levelArray.Length)
        {
            Debug.Log("no new level");
        }
        else
        {
            level++;
            doorPos = GameObject.Find("door").GetComponent<doorScript>().transform.position;
            player.GetComponent<player>().playerCheckpointPos = doorPos;
            StartCoroutine(loadLevel(level));
            //Destroy(currentLevel);
            //currentLevel = Instantiate(levelArray[level], new Vector3(0f, 0f, 0f), Quaternion.identity);
        }
    }

    IEnumerator loadLevel(int level)
    {
        transition.SetTrigger("transitionTrigger");

        yield return new WaitForSeconds(0.3f);

        Destroy(currentLevel);
        currentLevel = Instantiate(levelArray[level], new Vector3(0f, 0f, 0f), Quaternion.identity);
        player.transform.position = player.GetComponent<player>().playerCheckpointPos;
    }

}
