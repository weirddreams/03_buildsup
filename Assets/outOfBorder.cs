using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class outOfBorder : MonoBehaviour
{
    public GameObject player;
    public levelManager levelManager;
    // Start is called before the first frame update
    void Start()
    {
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag == "Player") // restart
        {
            collider.GetComponent<player>().superHeroLanding = false;
            levelManager.resetLevel();
        }
        else
        {
            Destroy(collider.gameObject);
        }
    }
}
